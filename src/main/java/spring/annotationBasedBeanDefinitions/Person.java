package spring.annotationBasedBeanDefinitions;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Person {

	private String name;
    private String address;
    private int age;

	public Person() {
		System.out.println("constructor of Person");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@PostConstruct
	public void postConstruct() {
		System.out.println("Person with name " + name + " is constructed");
	}

	@PreDestroy
	public void preDestroy() {
		System.out.println("Person with name " + name + " will be destroyed soon");
	}

	@Override
	public String toString() {
        return "{name: " + name + ", address: " + address + ", age: " + age + "}";
    }

}