package spring.xmlBeanDefinitions;

import spring.xmlBeanDefinitions.testClasses.PersonStorage;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InjectCollectionsTest {
    @Test
    public void injectCollectionTest() {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "xmlBeanDefinitions/injectCollections.xml");

        PersonStorage storage = (PersonStorage) context.getBean("PersonStorageBean");
        System.out.println(storage);
    }
}
